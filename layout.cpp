#include "layout.h"

const LayoutPixelArea $none = LayoutPixelArea::None;
const LayoutPixelArea $logoLeft = LayoutPixelArea::LogoLeft;
const LayoutPixelArea $logoRight = LayoutPixelArea::LogoRight;
const LayoutPixelArea $barLeft = LayoutPixelArea::BarLeft;
const LayoutPixelArea $barRight = LayoutPixelArea::BarRight;
const LayoutPixelArea $pyramix = LayoutPixelArea::Pyramix;
const LayoutPixelArea $studios = LayoutPixelArea::Studios;

constexpr LayoutPixel $(float32_t row, float32_t column, LayoutPixelArea area)
{
  return LayoutPixel {};
}

constexpr LayoutPixel $(float32_t row, float32_t column)
{
  return $(row, column, $none);
}

const LayoutPixel layout[] =
{
  $(0, 0),
};
