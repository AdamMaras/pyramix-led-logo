#ifndef INFRASTRUCTURE_H
#define INFRASTRUCTURE_H

#include <EventResponder.h>
#include <SPI.h>

#define SPI_CLOCK_SPEED 10000000

#define BUFFER_SIZE(LED_COUNT) (4 + ((LED_COUNT) * 4) + 4 + ((LED_COUNT) / 16 + 1))

class PixelData
{
private:
  uint8_t _globalSettings;
  
  uint8_t _blue;
  uint8_t _green;
  uint8_t _red;

public:
  PixelData()
  {
    this->brightness(255);
    
    this->red(0);
    this->green(0);
    this->blue(0);
  }

  PixelData(uint8_t brightness, uint8_t red, uint8_t green, uint8_t blue)
  {
    this->brightness(brightness);
    
    this->red(red);
    this->green(green);
    this->blue(blue);
  }

  uint8_t brightness() const { return this->_globalSettings << 2; }
  
  uint8_t red() const { return this->_red; }
  uint8_t green() const { return this->_green; }
  uint8_t blue() const { return this->_blue; }

  void brightness(uint8_t brightness) { this->_globalSettings = 0b11000000 | (brightness >> 2); }
  
  void red(uint8_t red) { this->_red = red; }
  void green(uint8_t green) { this->_green = green; }
  void blue(uint8_t blue) { this->_blue = blue; }
};

class ChannelDefinitionBase
{
public:
  virtual uint32_t count() = 0;
  virtual PixelData* pixel(uint32_t index) = 0;
  virtual bool startTransfer() = 0;
  virtual volatile bool readyForNextTransfer() = 0;
  
  virtual void notifyTransferComplete() = 0;
};

extern ChannelDefinitionBase* channels[];

extern uint8_t channelCount;

void initializeChannelCount();

#endif // INFRASTRUCTURE_H
