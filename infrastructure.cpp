#include "infrastructure.h"

void eventResponderHandler(EventResponder& eventResponder)
{
  ChannelDefinitionBase* channel = (ChannelDefinitionBase*) eventResponder.getContext();
  channel->notifyTransferComplete();
}

template<uint32_t LED_COUNT>
class ChannelDefinition : public ChannelDefinitionBase
{
private:
  uint8_t buffer0[BUFFER_SIZE(LED_COUNT)];
  uint8_t buffer1[BUFFER_SIZE(LED_COUNT)];

  volatile uint8_t * workBuffer = buffer0;
  
  SPIClass * spi;

  EventResponder eventResponder;
  volatile bool idle = true;

public:
  ChannelDefinition(SPIClass &spi)
  {
    this->spi = &spi;

    this->eventResponder.setContext(this);
    this->eventResponder.attachImmediate(eventResponderHandler);

    // start frame
    for (uint32_t index = 0; index < 4; index++)
    {
      this->buffer0[index] = 0x00;
      this->buffer1[index] = 0x00;
    }

    // individual pixels
    for (uint32_t index = 0; index < LED_COUNT; index++)
    {
      *((PixelData *) &this->buffer0[4 + (index * 4)]) = PixelData();
      *((PixelData *) &this->buffer1[4 + (index * 4)]) = PixelData();
    }

    // end frame
    for (uint32_t index = 4 + (LED_COUNT * 4); index < BUFFER_SIZE(LED_COUNT); index++)
    {
      this->buffer0[index] = 0x00;
      this->buffer1[index] = 0x00;
    }
  }

  uint32_t count() { return LED_COUNT; }
  
  PixelData* pixel(uint32_t index) { return (PixelData *) &this->workBuffer[4 + (index * 4)]; }

  bool startTransfer()
  {
    this->idle = false;

    uint8_t * previousWorkBuffer = (uint8_t *) this->workBuffer;
    this->workBuffer = this->workBuffer == this->buffer0 ? this->buffer1 : this->buffer0;

    spi->beginTransaction(SPISettings(SPI_CLOCK_SPEED, MSBFIRST, SPI_MODE0));
    bool status = this->spi->transfer((void *) previousWorkBuffer, nullptr, BUFFER_SIZE(LED_COUNT), this->eventResponder);

    return status;
  }

  volatile bool readyForNextTransfer() { return this->idle; }

  void notifyTransferComplete() { this->idle = true; spi->endTransaction(); }
};

ChannelDefinition<600> channel0(SPI);
ChannelDefinition<600> channel1(SPI1);
ChannelDefinition<600> channel2(SPI2);

ChannelDefinitionBase* channels[] = {
  &channel0,
  &channel1,
  &channel2,
  nullptr
};

uint8_t channelCount = 0;

void initializeChannelCount()
{
  while (channels[channelCount] != nullptr) { channelCount++; }
}
