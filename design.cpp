#include "design.h"
#include "infrastructure.h"

constexpr uint8_t mix(uint8_t left, uint8_t right, uint8_t mixAmount)
{
  left *= float32_t(UINT8_MAX - mixAmount);
  right *= float32_t(mixAmount);
  
  return left + right;
}

// PATTERNS ////////////////////////////////////////////////////////////////////

void flatWhite(const LayoutPixel &, PixelData & pixel, uint16_t, float32_t)
{
  pixel = PixelData(255, 255, 255, 255);
}

void flatBlack(const LayoutPixel &, PixelData & pixel, uint16_t, float32_t)
{
  pixel = PixelData(0, 0, 0, 0);
}

const PatternFunction patterns[] = {
  flatWhite, flatBlack
};

// TRANSITIONS /////////////////////////////////////////////////////////////////

void fade(const LayoutPixel &, const PixelData & left, const PixelData & right, PixelData & result, float32_t progress)
{
  uint8_t mixAmount = uint8_t(progress * UINT8_MAX);
  
  result.brightness(mix(left.brightness(), right.brightness(), mixAmount));

  result.red(mix(left.red(), right.red(), mixAmount));
  result.green(mix(left.green(), right.green(), mixAmount));
  result.blue(mix(left.blue(), right.blue(), mixAmount));
}

const TransitionFunction transitions[] = {
  fade
};
