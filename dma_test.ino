#include "infrastructure.h"
#include "layout.h"
#include "design.h"

void setup() {
  SPI.begin();
  SPI1.begin();
  SPI2.begin();
  
  initializeChannelCount();
}

void loop() {
  uint32_t overallPixelIndex = 0;
  for (uint8_t channelIndex = 0; channelIndex < channelCount; channelIndex++)
  {
    for (uint32_t pixelIndex = 0; pixelIndex < channels[channelIndex]->count(); pixelIndex++)
    {
      PixelData* pixel = channels[channelIndex]->pixel(pixelIndex);
      
      //*pixel = PixelData(255, channelIndex % 256, pixelIndex % 256, overallPixelIndex % 256);
      uint8_t val = millis() / 10 % 256;
      *pixel = PixelData(16, val, val, val);

      overallPixelIndex++;
    }
  }

  // wait for channels to go idle
  for (uint8_t channelIndex = 0; channelIndex < channelCount; channelIndex++)
  {
    while(!channels[channelIndex]->readyForNextTransfer()) { }
  }

  // start transfers
  for (uint8_t channelIndex = 0; channelIndex < channelCount; channelIndex++)
  {
    channels[channelIndex]->startTransfer();
  }
}
