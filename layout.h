#ifndef LAYOUT_H
#define LAYOUT_H

typedef float  float32_t;
typedef double float64_t;

enum LayoutPixelArea
{
  None =      0b000,
  
  LogoLeft =  0b010,
  LogoRight = 0b011,

  BarLeft =   0b100,
  BarRight =  0b101,

  Pyramix =   0b110,
  Studios =   0b111,
};

struct LayoutPixel
{
public:
  float32_t row;
  float32_t column;

  LayoutPixelArea area;
};

extern const LayoutPixel layout[];

#endif // LAYOUT_H
