#ifndef DESIGN_H
#define DESIGN_H

#include "infrastructure.h"
#include "layout.h"

typedef void (*PatternFunction) (const LayoutPixel & layout, PixelData & result, uint16_t seed, float32_t progress);
typedef void (*TransitionFunction) (const LayoutPixel & layout, const PixelData & left, const PixelData & right, PixelData & result, float32_t progress);

extern const PatternFunction patterns[];
extern const TransitionFunction transitions[];

#endif // DESIGN_H
